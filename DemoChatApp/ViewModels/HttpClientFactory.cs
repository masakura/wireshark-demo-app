﻿using System;
using System.Configuration;
using System.Net.Http;

namespace DemoChatApp.ViewModels
{
    internal static class HttpClientFactory
    {
        public static HttpClient Create()
        {
            return new HttpClient
            {
                BaseAddress = new Uri(ConfigurationManager.AppSettings.Get("ApiUrl")),
                Timeout = TimeSpan.FromSeconds(30)
            };
        }
    }
}