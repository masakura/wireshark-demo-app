﻿using Prism.Mvvm;

namespace DemoChatApp.ViewModels
{
    public sealed class ChatApp : BindableBase
    {
        public ChatApp()
        {
            Client = new ChatClient(HttpClientFactory.Create());
            Post = new ChatPost(Client);
            Timeline = new Timeline(Client);
        }

        public ChatPost Post { get; }
        public Timeline Timeline { get; }
        public ChatClient Client { get; }
    }
}