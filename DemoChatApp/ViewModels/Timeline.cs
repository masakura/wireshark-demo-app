﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Prism.Commands;
using Prism.Mvvm;

namespace DemoChatApp.ViewModels
{
    public sealed class Timeline : BindableBase
    {
        private readonly ChatClient _client;
        private readonly ObservableCollection<Post> _posts = new ObservableCollection<Post>();

        public Timeline(ChatClient client)
        {
            client.OnPost += Client_OnPost;
            client.PropertyChanged += (sender, args) => RaisePropertyChanged(nameof(RefreshCommand));
            _client = client;
        }

        public IEnumerable<Post> Items => _posts;

        public ICommand RefreshCommand => new DelegateCommand(async () => await RefreshAsync(), () => !_client.IsLoading);

        private void Client_OnPost(object sender, EventArgs e)
        {
#pragma warning disable 4014
            RefreshAsync();
#pragma warning restore 4014
        }

        public async Task RefreshAsync()
        {
            var posts = await _client.GetPostsAsync();
            if (posts == null) return;
            _posts.Clear();
            _posts.AddRange(posts);
        }
    }
}