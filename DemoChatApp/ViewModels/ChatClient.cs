﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Windows;
using Newtonsoft.Json;
using Prism.Mvvm;

namespace DemoChatApp.ViewModels
{
    public sealed class ChatClient : BindableBase
    {
        private readonly HttpClient _client;
        private int _loadingCount;

        public ChatClient(HttpClient client)
        {
            _client = client ?? throw new ArgumentNullException(nameof(client));
        }

        private int LoadingCount
        {
            get => _loadingCount;
            set
            {
                _loadingCount = value;
                if (_loadingCount < 0) _loadingCount = 0;

                RaisePropertyChanged(nameof(IsLoading));
            }
        }

        public bool IsLoading => LoadingCount > 0;

        private async Task<T> WrapLoading<T>(Func<Task<T>> action)
        {
            try
            {
                LoadingCount++;

                return await action();
            }
            finally
            {
                LoadingCount--;
            }
        }

        private async Task WrapLoading(Func<Task> action)
        {
            await WrapLoading<object>(async () =>
            {
                await action();
                return null;
            });
        }

        public async Task PostAsync(Post input)
        {
            try
            {
                await WrapLoading(async () =>
                {
                    var body = JsonConvert.SerializeObject(input);
                    var content = new StringContent(body);
                    content.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json");
                    var response = await _client.PostAsync("posts", content);

                    response.EnsureSuccessStatusCode();
                });

                OnPost?.Invoke(this, EventArgs.Empty);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);

                ShowError("エラーが発生しました");
            }
        }

        public async Task<IEnumerable<Post>> GetPostsAsync()
        {
            try
            {
                return await WrapLoading(async () =>
                {
                    var body = await _client.GetStringAsync("posts");
                    return JsonConvert.DeserializeObject<IEnumerable<Post>>(body);
                });
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);

                ShowError("エラーが発生しました");
                return null;
            }
        }

        private static void ShowError(string message)
        {
            MessageBox.Show(message, "エラー");
        }

        public event EventHandler OnPost;
    }
}