﻿using System;

namespace DemoChatApp.ViewModels
{
    public class Post
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Message { get; set; }
        public DateTime Timestamp { get; set; }
    }
}