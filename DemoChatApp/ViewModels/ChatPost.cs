﻿using System.Threading.Tasks;
using System.Windows.Input;
using Prism.Commands;
using Prism.Mvvm;

namespace DemoChatApp.ViewModels
{
    public sealed class ChatPost : BindableBase
    {
        private readonly ChatClient _client;
        private string _input;
        private string _name = "太郎";

        public ChatPost(ChatClient client)
        {
            client.PropertyChanged += (sender, args) => RaisePropertyChanged(nameof(PostCommand));
            _client = client;
        }

        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                RaisePropertyChanged();
            }
        }

        public string Input
        {
            get => _input;
            set
            {
                _input = value;
                RaisePropertyChanged();
            }
        }

        public ICommand PostCommand => new DelegateCommand(async () => await PostAsync(), () => !_client.IsLoading);

        private async Task PostAsync()
        {
            await _client.PostAsync(new Post()
            {
                Name = Name,
                Message = Input,
            });

            Input = "";
        }
    }
}