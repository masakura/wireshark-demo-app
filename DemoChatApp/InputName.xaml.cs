﻿using System.Windows;

namespace DemoChatApp
{
    /// <summary>
    ///     InputName.xaml の相互作用ロジック
    /// </summary>
    public partial class InputName : Window
    {
        public static readonly DependencyProperty UserNameProperty = DependencyProperty.Register(
            "UserName", typeof(string), typeof(InputName), new PropertyMetadata(default(string)));

        public InputName()
        {
            InitializeComponent();
        }

        public string UserName
        {
            get => (string) GetValue(UserNameProperty);
            set => SetValue(UserNameProperty, value);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}