﻿using System;
using DemoChatApp.ViewModels;

namespace DemoChatApp
{
    /// <summary>
    ///     MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private ChatApp AppData => (ChatApp) DataContext;

        private bool InputUserName()
        {
            var dialog = new InputName();
            dialog.ShowDialog();
            if (string.IsNullOrEmpty(dialog.UserName)) return false;

            AppData.Post.Name = dialog.UserName.Trim();
            return true;
        }

        protected override void OnContentRendered(EventArgs e)
        {
            base.OnContentRendered(e);

            if (!InputUserName()) Close();

#pragma warning disable 4014
            AppData.Timeline.RefreshAsync();
#pragma warning restore 4014
        }
    }
}