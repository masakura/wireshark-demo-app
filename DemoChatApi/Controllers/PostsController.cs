﻿using System;
using System.Collections.Generic;
using System.Linq;
using DemoChatApi.Services;
using Microsoft.AspNetCore.Mvc;

namespace DemoChatApi.Controllers
{
    [Route("api/[controller]")]
    public sealed class PostsController : ControllerBase
    {
        private readonly ApplicationDbContext _db;
        private readonly SimulationErrorService _errors;

        public PostsController(ApplicationDbContext db, SimulationErrorService errors)
        {
            _db = db;
            _errors = errors;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Post>> Get()
        {
            _errors.EnsureError();

            return _db.Posts.OrderByDescending(post => post.Timestamp)
                .Take(50)
                .ToArray();
        }

        [HttpPost]
        public void Post([FromBody] Input post)
        {
            _errors.EnsureError();

            post.ThrowIfNotValid();

            _db.Posts.Add(post.ToPost());
            _db.SaveChanges();
        }

        public sealed class Input
        {
            public string Name { get; set; }
            public string Message { get; set; }

            public Post ToPost()
            {
                return new Post
                {
                    Name = Name,
                    Message = Message,
                    Timestamp = DateTime.Now
                };
            }

            public void ThrowIfNotValid()
            {
                if (string.IsNullOrWhiteSpace(Name) || string.IsNullOrWhiteSpace(Message))
                    throw new InvalidOperationException("名前とメッセージの両方が必要です。");
            }
        }
    }
}