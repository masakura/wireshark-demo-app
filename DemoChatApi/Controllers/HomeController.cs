﻿using Microsoft.AspNetCore.Mvc;

namespace DemoChatApi.Controllers
{
    public class HomeController : Controller
    {
        // GET
        public IActionResult Index()
        {
            return View();
        }
    }
}