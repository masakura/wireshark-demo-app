﻿using DemoChatApi.Services;
using Microsoft.AspNetCore.Mvc;

namespace DemoChatApi.Controllers
{
    public sealed class AdminController : Controller
    {
        private readonly SimulationErrorService _errors;

        public AdminController(SimulationErrorService errors)
        {
            _errors = errors;
        }

        // GET
        [HttpGet]
        public IActionResult Index()
        {
            return View(new AdminSetting {RandomMax = _errors.RandomMax});
        }

        [HttpPost]
        public IActionResult Index([FromForm] AdminSetting setting)
        {
            _errors.RandomMax = setting.RandomMax;
            return RedirectToAction("Index");
        }

        public sealed class AdminSetting
        {
            public int RandomMax { get; set; }
        }
    }
}