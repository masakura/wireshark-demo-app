﻿using System;
using System.Threading;

namespace DemoChatApi.Services
{
    public sealed class SimulationErrorService
    {
        private readonly Random _random = new Random();

        public int RandomMax { get; set; } = 0;

        public void EnsureError()
        {
            if (RandomMax <= 0) return;

            var value = _random.Next(RandomMax);
            switch (value)
            {
                case 0: throw new InvalidOperationException("エラー");
                case 1:
                    Thread.Sleep(1000 * 60 * 10);
                    break;
            }
        }
    }
}