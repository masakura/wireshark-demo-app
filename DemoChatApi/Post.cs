﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DemoChatApi
{
    public sealed class Post
    {
        [Key] public int Id { get; set; }

        public string Name { get; set; }
        public string Message { get; set; }
        public DateTime Timestamp { get; set; }
    }
}