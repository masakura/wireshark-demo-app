﻿using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace UdpSender
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var options =
                new
                {
                    Bytes = Encoding.UTF8.GetBytes(args[0]),
                    DestinationIP = args[1],
                    Port = int.Parse(args[2]),
                    Interval = int.Parse(args[3])
                };

            using (var udp = new UdpClient())
            {
                while (true)
                {
                    foreach (var b in options.Bytes)
                    {
                        udp.Send(new[] {b}, 1, options.DestinationIP, options.Port);
                        Thread.Sleep(options.Interval);
                    }
                }
            }
        }
    }
}